const sampleTable = 'Name\tMRN\tDate\tType\tStudy'
  + '\nFirstName LastName\t12345\tMM/DD/YYYY\tSome Cells\tCLLBANK'
  + '\nReport Bugs\t111\t09/21/2020\tSALIVA\tCLLBANK'
  + '\nTo Me\t222\t09/22/2020\tSERUM\tCLLBANK'
  + '\nJennings Zhang\t333\t09/23/2020\tPBMC\tCLLBANK';

const sampleTemplate = '{MRN}\n{Name}\n{Date}    {Type}\n{Study}';

export { sampleTable, sampleTemplate };
