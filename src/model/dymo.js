/*
 * Writes .dymo files (actually proprietary XML) for DYMO label printer DYMO Connect software.
 */

/**
 * Wrap a string in an XML opening and closing tag.
 * @param tag
 * @param inner
 * @return {string}
 */
function e(tag, inner) {
  return `<${tag}>${inner}</${tag}>`;
}

/**
 * Sets up label size and metadata.
 * @param body
 * @param xmlDataTable
 * @return {string}
 */
function outerXml(body, xmlDataTable) {
  return `<?xml version="1.0" encoding="utf-8"?>
<DesktopLabel Version="1">
  <DYMOLabel Version="3">
    <Description>DYMO Label</Description>
    <Orientation>Landscape</Orientation>
    <LabelName>1738595 DYMO Barcode</LabelName>
    <InitialLength>0</InitialLength>
    <BorderStyle>SolidLine</BorderStyle>
    <DYMORect>
      <DYMOPoint>
        <X>0.2266667</X>
        <Y>0.05666666</Y>
      </DYMOPoint>
      <Size>
        <Width>2.213333</Width>
        <Height>0.6533333</Height>
      </Size>
    </DYMORect>
    <BorderColor>
      <SolidColorBrush>
        <Color A="1" R="0" G="0" B="0"></Color>
      </SolidColorBrush>
    </BorderColor>
    <BorderThickness>1</BorderThickness>
    <Show_Border>False</Show_Border>
    <DynamicLayoutManager>
      <RotationBehavior>ClearObjects</RotationBehavior>
      <LabelObjects>
        <TextObject>
          <Name>TEXT</Name>
          <Brushes>
            <BackgroundBrush>
              <SolidColorBrush>
                <Color A="0" R="1" G="1" B="1"></Color>
              </SolidColorBrush>
            </BackgroundBrush>
            <BorderBrush>
              <SolidColorBrush>
                <Color A="1" R="0" G="0" B="0"></Color>
              </SolidColorBrush>
            </BorderBrush>
            <StrokeBrush>
              <SolidColorBrush>
                <Color A="1" R="0" G="0" B="0"></Color>
              </SolidColorBrush>
            </StrokeBrush>
            <FillBrush>
              <SolidColorBrush>
                <Color A="0" R="0" G="0" B="0"></Color>
              </SolidColorBrush>
            </FillBrush>
          </Brushes>
          <Rotation>Rotation0</Rotation>
          <OutlineThickness>1</OutlineThickness>
          <IsOutlined>False</IsOutlined>
          <BorderStyle>SolidLine</BorderStyle>
          <Margin>
            <DYMOThickness Left="0" Top="0" Right="0" Bottom="0" />
          </Margin>
          <HorizontalAlignment>Left</HorizontalAlignment>
          <VerticalAlignment>Top</VerticalAlignment>
          <FitMode>AlwaysFit</FitMode>
          <IsVertical>False</IsVertical>
          <FormattedText>
            <FitMode>AlwaysFit</FitMode>
            <HorizontalAlignment>Left</HorizontalAlignment>
            <VerticalAlignment>Top</VerticalAlignment>
            <IsVertical>False</IsVertical>
            ${body}
          </FormattedText>
          <ObjectLayout>
            <DYMOPoint>
              <X>0.3343524</X>
              <Y>0.07833333</Y>
            </DYMOPoint>
            <Size>
              <Width>1.318793</Width>
              <Height>0.5933333</Height>
            </Size>
          </ObjectLayout>
        </TextObject>
      </LabelObjects>
    </DynamicLayoutManager>
  </DYMOLabel>
  <LabelApplication>Blank</LabelApplication>
  ${xmlDataTable}
</DesktopLabel>`;
}

/**
 * Convert matrix to DYMO-XML <DataTable> element.
 *
 * @param dataMatrix
 * @return {string}
 */
function matrix2xml(dataMatrix) {
  // eslint-disable-next-line no-param-reassign
  const matrix = [...dataMatrix];

  const tableHeader = matrix.shift();
  const columnsXml = tableHeader.map((s) => e('DataColumn', s)).join('');

  const rowsXml = matrix.map((row) => e('DataRow', row.map((s) => e('Value', s)).join(''))).join('');
  return e('DataTable', e('Columns', columnsXml) + e('Rows', rowsXml));
}

/**
 * Produce XML for a value which is filled from the data table.
 *
 * @param columnName
 * @param placeholder
 * @return {string}
 */
function xmlVariable(columnName, placeholder) {
  return `              <DataMappingTextSpan>
                <Text>${placeholder}</Text>
                <FontInfo>
                  <FontName>Book Antiqua</FontName>
                  <FontSize>6.6</FontSize>
                  <IsBold>False</IsBold>
                  <IsItalic>False</IsItalic>
                  <IsUnderline>False</IsUnderline>
                  <FontBrush>
                    <SolidColorBrush>
                      <Color A="1" R="0" G="0" B="0"></Color>
                    </SolidColorBrush>
                  </FontBrush>
                </FontInfo>
                <ColumnName>${columnName}</ColumnName>
              </DataMappingTextSpan>`;
}

/**
 * Produce XML for a string that does not change.
 *
 * @param text
 * @return {string}
 */
function xmlText(text) {
  return `              <TextSpan>
                <Text>${text}</Text>
                <FontInfo>
                  <FontName>Book Antiqua</FontName>
                  <FontSize>6.6</FontSize>
                  <IsBold>False</IsBold>
                  <IsItalic>False</IsItalic>
                  <IsUnderline>False</IsUnderline>
                  <FontBrush>
                    <SolidColorBrush>
                      <Color A="1" R="0" G="0" B="0"></Color>
                    </SolidColorBrush>
                  </FontBrush>
                </FontInfo>
              </TextSpan>`;
}

/**
 * Convert user input layout string template to the body content
 * of the label, represented by several DYMO-XML <LineTextSpan>.
 */
function inflateTemplate(matrix, template, firstRow) {
  return template.map((tokenizedLine) => e('LineTextSpan', tokenizedLine.map((token) => {
    if (token.startsWith('$')) {
      const variable = token.substring(1);
      const placeholder = firstRow[variable];
      if (!placeholder) {
        throw Error(`column "${variable}" not found`);
      }
      return xmlVariable(variable, placeholder);
    }
    return xmlText(token);
  }).join(''))).join('');
}

/**
 * Generate entire .dymo file XML syntax from the model.
 * @param {LabelaborModel} model
 */
function generateDymoXml(model) {
  return outerXml(
    inflateTemplate(model.matrix, model.tokenizedTemplate, model.firstRow),
    matrix2xml(model.matrix),
  );
}

export default generateDymoXml;
