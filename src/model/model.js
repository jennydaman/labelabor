/* eslint-disable no-restricted-syntax,no-plusplus */

/**
 * Check syntax.
 *
 * @param printTemplate
 * @return {boolean} true if curlies are invalid
 */
function assertCurlies(printTemplate) {
  let inside = false;
  for (const char of printTemplate.split('')) {
    if (char === '{') {
      if (inside) {
        throw new Error('found two "{" in a row');
      }
      inside = true;
    } else if (char === '}') {
      if (!inside) {
        throw new Error('found a "}" without a "{" before');
      }
      inside = false;
    } else if (char === '\n' && inside) {
      throw new Error('found line break after "{"');
    } else if (char === '$') {
      throw new Error(`found illegal character "${char}"`);
    }
  }
  if (inside) {
    throw new Error('missing "}"');
  }
}

export default class LabelerModel {
  /**
   * Example:
   *
   *       --
   *     [
   *         [ '$MRN' ],
   *         [ '$Name' ],
   *         [ '$Date', '    ', '$Type' ],
   *         [ '$Study' ]
   *     ]
   */
  tokenizedTemplate = [[]];

  matrix = [];

  firstRow = {};

  /**
   * Model objects should be treated as immutable.
   * To update any of its fields, invoke this constructor.
   *
   * Validity of printTemplate depends on the value of tsv
   * and vice versa, so all fields must be updated in one go
   * and should not be changed individually.
   *
   * @param tsv
   * @param printTemplate
   */
  constructor(tsv, printTemplate) {
    this.matrix = LabelerModel.parseTsv(tsv);
    this.firstRow = LabelerModel.firstRow2dict(this.matrix);
    /*
     * matrix must be set before printTemplate
     * because printTemplate will check to see
     * if variable names are valid column names,
     * which is also why printTemplate is non-static
     */
    this.printTemplate = printTemplate;
  }

  /**
   * Tokenize user-inputted print template and assert that the
   * variable names it uses are found as column names in the data table.
   * Variable representation is changed from {variable} to $variable,
   * just to save code when using substring later on.
   *
   * @param {string} inputPrintTemplate example: '{MRN}\n{Name}\n{Date}    {Type}\n{Study}'
   */
  set printTemplate(inputPrintTemplate) {
    assertCurlies(inputPrintTemplate);

    const lines = inputPrintTemplate.trim().split('\n');
    this.tokenizedTemplate = lines.map((line) => {
      const frags = line.split('}');
      const nestedTokens = frags.map((frag) => {
        const a = frag.split('{');
        if (a.length > 1) {
          const variable = a[1];
          if (!this.firstRow[variable]) {
            throw Error(`column "${variable}" not found`);
          }
          // eslint-disable-next-line prefer-template
          a[1] = '$' + variable;
        }
        return a;
      });
      return [].concat(...nestedTokens).filter((s) => s !== '');
    });
  }

  /**
   * Converts tab-separated values in a string to a matrix.
   * Also construct a dictionary called firstRow, mapping column names
   * to values in the first row.
   *
   * @param {string} tsv user input tsv, copied from Excel or CLLDB
   */
  static parseTsv(tsv) {
    const lines = tsv.trim().split('\n');
    if (lines.length < 2) {
      throw Error('tsv data does not have a header and first row');
    }
    const matrix = lines.map((line) => line.split('\t').map(this.processValue));

    // assert matrix is not jagged
    const correctNumCols = matrix[0].length;
    for (let i = 1; i < matrix.length; i++) {
      const numCols = matrix[i].length;
      if (numCols !== correctNumCols) {
        throw new Error(`row ${i} has ${numCols} columns, expected ${correctNumCols}`);
      }
    }

    return matrix;
  }

  /**
   * Cleans up a string value by removing leading and trailing whitespace.
   * If the value is 'NA' then it gets replaced by an empty string.
   * @param {string} word
   */
  static processValue(word) {
    const trimmed = word.trim();
    if (trimmed === 'NA') {
      return ' ';
    }
    return trimmed;
  }

  static firstRow2dict(matrix) {
    const firstRow = {};
    for (let i = 0; i < matrix[0].length; i++) {
      firstRow[matrix[0][i]] = matrix[1][i];
    }
    return firstRow;
  }

  get preview() {
    return this.tokenizedTemplate.map((l) => l.map((t) => (t.startsWith('$') ? this.firstRow[t.substring(1)] : t)).join('')).join('\n');
  }
}
