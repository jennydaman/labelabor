const yesterday = new Date();
yesterday.setDate(yesterday.getDate() - 1);
const day = yesterday.toLocaleDateString();

export default [
  {
    preview: '123456, MY-FAM\nJennings Zhang\n'
      + `Date of Isolation:   ${day}\n`
      + 'Date of Collection: 10/21/2020'
      + '\nSALIVA DNA     800 ng/μL',
    template: '{MRN}, {Family Code}\n'
      + '{First} {Last}\n'
      + 'Date of Isolation:   {Date Isolated}\n'
      + 'Date of Collection: {Date on Sample}\n'
      + 'SALIVA DNA     {Concentration (ng/uL)} ng/μL',
    name: 'Saliva',
  },
  {
    preview: '123456\nJennings Zhang\n11/20/2020 B-CELLS DNA ALL PREP\nDate Isolated: 10/23/2020 54-321 C26D1\n67.8 ng/μL',
    template: '{MRN}\n{First Name} {Last Name}\n{date} DNA {Prep}\nDate Isolated: {date iso} {prot} {cycle}\n{concentration} ng/μL',
    name: 'DNA',
  },
  {
    preview: '123456\nJennings Zhang\n11/20/2020 B-CELLS RNA ALL PREP\nDate Isolated: 10/23/2020 54-321 C26D1\n67.8 ng/μL RIN 8.90',
    template: '{MRN}\n{First Name} {Last Name}\n{date} RNA {Prep}\nDate Isolated: {date iso} {prot} {cycle}\n{concentration} ng/μL RIN {RIN}',
    name: 'RNA',
  },
  {
    preview: 'please ask Jennings\nto add your label\ntemplate here!',
    template: 'please ask Jennings\nto add your label\ntemplate here!',
    name: 'other',
  },
];
