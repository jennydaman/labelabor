const { URL } = require('url');

module.exports = {
  lintOnSave: false,
  transpileDependencies: [
    'vuetify',
  ],
};

if (process.env.CI_PAGES_URL) {
  module.exports.publicPath = new URL(process.env.CI_PAGES_URL).pathname;
}
