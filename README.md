# 3rd-Party DYMO Label Editor

https://jennydaman.gitlab.io/labelabor

Third-party DYMO label-printer editor for printing many labels.
It's mainly helpful because you can copy-and-paste from Excel.

Developed for the [Brown Lab](https://brownlab.dana-farber.org/)
at Dana-Farber Cancer Institute.

# Development

```bash
# 0. download
git clone https://gitlab.com/jennydaman/labelabor.git
cd labelabor
# 1. install dependencies
yarn
# 2. start a hot-reload server
yarn serve
```

# Contributing

Pre-defined label templates can be added to the file
[src/model/templates.js](https://gitlab.com/jennydaman/labelabor/-/blob/master/src/model/templates.js).
